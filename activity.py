# 1. Create a car dictionary with the following keys: brand, model, year of make, color (any values)

car = {
    "brand": "Honda",
    "model": "Civic Type R",
    "year_of_make": "1996",
    "color": "White",
}

# 2. Print the following statement from the details: "I own a (color) (brand) (model) and it was made in (year_of_make)"

counter = 0
while counter < 1:
    print(
        f"I own a {car['color']} {car['brand']} and it was made in {car['year_of_make']}")
    counter += 1


# 3. Create a function that gets the square of a number

def square(number):
    return number * 2


number_square = square(2)
print(number_square)

# 4. Create a function that takes one of the following languages as its parameter:

# a. French
# b. Spanish
# c. Japanese


# Depending on which language is given, the function prints "Hello World!" in that language. Add a condition that asks the user to input a valid language if the given parameter does not match any of the above.

def languages(input_lang):
    if input_lang == "French":
        print('The french translation for "Hello world!" is Bonjour le monde!')
    elif input_lang == "Spanish":
        print('The spanish translation for "Hello world!" is Hola mundo!')
    elif input_lang == "Japanese":
        print("The japanese translation for 'Hello World! ' is こんにちは、世界よ(Kon'nichiwa, sekai yo)")
    else:
        print('Error! Invalid input. Please try again.')


input_language = languages(
    input('Enter language(French, Spanish, Japanese):\n'))

print(input_language)
